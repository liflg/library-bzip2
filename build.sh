#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        export CFLAGS="-ggdb"
        export CXXFLAGS="-ggdb"
    fi

    if [[ ( -z "$OPTIMIZATION" ) || ( "$OPTIMIZATION" -eq 2 ) ]]; then
        export CFLAGS="$CFLAGS -O2"
    else
        echo "Optimization level $OPTIMIZATION is not yet implemented!"
        exit 1
    fi

    rm -rf "$PREFIXDIR"

    ( cd source
      make -j "$(nproc)" -f Makefile-libbz2_so
      install -m 0755 -d "$PREFIXDIR"/include "$PREFIXDIR"/lib
      install -m 0644 bzlib.h "$PREFIXDIR"/include
      install -m 0755 libbz2.so.1.0.6 "$PREFIXDIR"/lib
      ln -s libbz2.so.1.0.6 "$PREFIXDIR"/lib/libbz2.so.1.0
      ln -s libbz2.so.1.0 "$PREFIXDIR"/lib/libbz2.so)

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout .)
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/LICENSE "$PREFIXDIR"/lib/LICENSE-bzip2.txt

rm -rf "$BUILDDIR"

echo "bzip2 for $MULTIARCHNAME is ready."
